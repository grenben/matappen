//
//  TableViewController.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()


@end

@implementation TableViewController

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchString];
    self.searchResult = [self.data filteredArrayUsingPredicate:resultPredicate];
    
    [self.tableView reloadData];
}

-(void)datacallback:(id)Data withIdentifier:(int)Identifier {
    
}

-(void)datacallback:(id)Data withIndexPath:(NSIndexPath*)indexPath {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSDictionary *result = Data;
        
        CellTableViewCell *thisCell = [self.tableView dequeueReusableCellWithIdentifier:@"CustomCell" forIndexPath:indexPath];
        thisCell.fatLabel.text = [NSString stringWithFormat:@"%@",result[@"fat"]];
        thisCell.proteinLabel.text = [NSString stringWithFormat:@"%@", result[@"protein"]];
        thisCell.carbohydrateLabel.text = [NSString stringWithFormat:@"%@", result[@"carbohydrates"]];
        thisCell.energyString = [NSString stringWithFormat:@"%@", result[@"energyKcal"]];
        thisCell.foodDict = result;
        
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        //[self.tableView reloadData];
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Register Class for Cell Reuse Identifier
    //[self.tableView registerClass:[CellTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    self.backend = [[Backend alloc] init];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.navigationItem.titleView = self.searchController.searchBar;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        return self.searchResult.count;
    }
    else {
        return self.data.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell" forIndexPath:indexPath];
    
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        NSDictionary *dataDict = self.searchResult[indexPath.row];
        cell.nutritionLabel.text = dataDict[@"name"];
        
    }
    else {
        NSDictionary *dataDict = self.data[indexPath.row];
        cell.nutritionLabel.text = dataDict[@"name"];
        //NSLog(@"indexPath%ld", (long)indexPath.row);
        [self.backend getFoodContent:self indexPath:indexPath];
    }
  
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        
        DetailViewController *detailViewController = [segue destinationViewController];
        CellTableViewCell *thisCell = sender;
        
        detailViewController.fat = thisCell.fatLabel.text;
        detailViewController.carbohydrate = thisCell.carbohydrateLabel.text;
        detailViewController.protein = thisCell.proteinLabel.text;
        detailViewController.nutrition = thisCell.nutritionLabel.text;
        detailViewController.foodDict = thisCell.foodDict;
        
        NSLog(@"energy %@", thisCell.energyString);
        detailViewController.energy = thisCell.energyString;
        
    }

}


@end
