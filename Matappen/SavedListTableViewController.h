//
//  SavedListTableViewController.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-31.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SavedListTableViewCell.h"
#import "DetailViewController.h"

@interface SavedListTableViewController : UITableViewController 

@end
