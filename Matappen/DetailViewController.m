//
//  DetailViewController.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property NSMutableDictionary *savedList;
@property int howGoodValue;
@property (weak, nonatomic) IBOutlet UIView *goodForYouView;
@property (weak, nonatomic) IBOutlet UILabel *goodForYouLabel;

@end

@implementation DetailViewController

- (void) lowCarbCheck {
    //NSString *carbsString = [NSString stringWithFormat:@"%@", self.foodDict[@"carbohydres"]];
    int carbs = [self.carbohydrate intValue];
    
    if (carbs <= 5) {
        [_goodForYouView setBackgroundColor:[UIColor greenColor]];
        _goodForYouLabel.text = @"STRICT LOWCARB";
    }
    else if (carbs > 5 && carbs <= 15) {
        [_goodForYouView setBackgroundColor:[UIColor greenColor]];
        _goodForYouLabel.text = @"MODERATE LOWCARB";
    }
    else {
        [_goodForYouView setBackgroundColor:[UIColor redColor]];
        _goodForYouLabel.text = @"NOT LOWCARB";
    }
    
}

- (IBAction)takePhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}


-(NSString *)imagePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    return [path stringByAppendingPathComponent:self.nutrition];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    self.photoView.image = image;
    
    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL succes = [imageData writeToFile:[self imagePath] atomically:YES];
    
    if (succes) {
        NSLog(@"saved image to user documents directory");
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        NSLog(@"Failed to save data");
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nutritionLabel.text = self.nutrition;
    self.fatLabel.text = self.fat;
    self.proteinLabel.text = self.protein;
    self.carbohydrateLabel.text = self.carbohydrate;
    //self.energyLabel.text = [NSString stringWithFormat:@"%@", self.foodDict[@"energyKcal"]];
    self.energyLabel.text = self.energy;
    
    if (![[[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys] containsObject:@"saved"]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:dict forKey:@"saved"];
        self.savedList = [userDefaults objectForKey:@"saved"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        self.savedList = [[[NSUserDefaults standardUserDefaults] objectForKey:@"saved"] mutableCopy];
        NSLog(@"ViewDidLoad savedList: %@", self.savedList);
    }
    
    
    
    UIImage *cachedImage = [UIImage imageWithContentsOfFile:[self imagePath]];
    if (cachedImage) {
        self.photoView.image = cachedImage;
    }
    else {
        NSLog(@"No cached image found");
    }
    
    [self lowCarbCheck];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)deleteFood:(UIBarButtonItem *)sender {
    
    [self.savedList removeObjectForKey:self.nutrition];
    [[NSUserDefaults standardUserDefaults] setObject:self.savedList forKey:@"saved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"deleteFood savedList: %@", self.savedList);
}

- (IBAction)saveFood:(UIBarButtonItem *)sender {
    
    NSMutableDictionary *foodItem = [[NSMutableDictionary alloc] init];
    [foodItem setObject:self.nutritionLabel.text forKey:@"nutrition"];
    [foodItem setObject:self.energyLabel.text forKey:@"energy"];
    [foodItem setObject:self.fatLabel.text forKey:@"fat"];
    [foodItem setObject:self.proteinLabel.text forKey:@"protein"];
    [foodItem setObject:self.carbohydrateLabel.text forKey:@"carbohydrates"];
    [foodItem setObject:self.foodDict forKey:@"foodDict"];
    
    [self.savedList setObject:foodItem forKey:self.nutrition];
    [[NSUserDefaults standardUserDefaults] setObject:self.savedList forKey:@"saved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"saveFood: savedList: %@", self.savedList);
}
@end
