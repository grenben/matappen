//
//  DetailViewController.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"

@interface DetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)takePhoto:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *nutritionLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbohydrateLabel;

@property NSDictionary *foodDict;

@property NSString *nutrition;
@property NSString *fat;
@property NSString *protein;
@property NSString *carbohydrate;
@property NSString *energy;


@end
