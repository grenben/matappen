//
//  DataCallback.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-19.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

@protocol DataCallback

-(void)datacallback:(id)Data withIdentifier:(int)Identifier;
-(void)datacallback:(id)Data withIndexPath:(NSIndexPath*)indexPath;

@end