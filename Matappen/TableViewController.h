//
//  TableViewController.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"
#import "CellTableViewCell.h"
#import "DetailViewController.h"
#import "SavedListTableViewController.h"

@interface TableViewController : UITableViewController <UISearchResultsUpdating, DataCallback>

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController;

@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSArray *searchResult;
@property UISearchController *searchController;
@property Backend *backend;



@end
