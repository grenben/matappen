//
//  SavedListTableViewController.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-31.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "SavedListTableViewController.h"

@interface SavedListTableViewController ()

@property NSMutableDictionary *savedList;
@property NSMutableArray *myKeys;
@property NSMutableArray *selectedItems;
@property NSMutableArray *selectedItemsName;

@property int nrOfSelectedCells;


@end

@implementation SavedListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self loadNSUserDefaults];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadNSUserDefaults];

    [self.tableView reloadData];
    
}

- (void)loadNSUserDefaults {
    self.savedList = [[[NSUserDefaults standardUserDefaults] objectForKey:@"saved"] mutableCopy];
    NSLog(@"SavedListTableView self.savedList: %@", self.savedList);
    
    self.myKeys = [[self.savedList allKeys] mutableCopy];
    NSLog(@"SavedListTableView self.myKeys: %@", self.myKeys);

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.myKeys.count;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    int a = (long) indexPath.row;
//    NSLog(@"index: %i",a);
//
//}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.accessoryType = UITableViewCellAccessoryNone;
//    }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SavedListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SavedListTableViewCell" forIndexPath:indexPath];
    
    NSString *cellString = self.myKeys[indexPath.row];
    
    NSDictionary *cellDictionary = self.savedList[cellString];
    
    cell.nutritrionLabel.text = cellDictionary[@"nutrition"];
    cell.fatLabel.text = cellDictionary[@"fat"];
    cell.proteinLabel.text = cellDictionary[@"protein"];
    cell.carbohydratesLabel.text = cellDictionary[@"carbohydrates"];
    cell.foodDict = cellDictionary[@"foodDict"];
    cell.energyString = cellDictionary[@"energy"];
    NSLog(@"SavedListTableViewCell energyString %@", cell.energyString);
    
//    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    } else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        NSMutableDictionary *savedList = [[[NSUserDefaults standardUserDefaults] objectForKey:@"saved"] mutableCopy];
//        [savedList removeObjectForKey:self.myKeys[indexPath.row]];
//        [self.myKeys removeObjectAtIndex:indexPath.row];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:savedList forKey:@"saved"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/




// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"detailSegue2"]) {
        
        DetailViewController *detailVC = [segue destinationViewController];
        SavedListTableViewCell *cell = sender;
        detailVC.nutrition = cell.nutritrionLabel.text;
        detailVC.fat = cell.fatLabel.text;
        detailVC.protein = cell.proteinLabel.text;
        detailVC.carbohydrate = cell.carbohydratesLabel.text;
        detailVC.energy = cell.energyString;
        NSLog(@"prepareForSegue cell.energyString: %@", cell.energyString);
        NSLog(@"prepareForSegue detailVC.energy: %@", detailVC.energy);
    }
}


@end
