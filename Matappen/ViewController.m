//
//  ViewController.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "ViewController.h"
#import "Backend.h"
#import "TableViewController.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *foodListButton;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@property UIView *protein;
@property UIView *carbs;
@property UIView *fat;
@property UIView *calori;

@property UIView *protein1;
@property UIView *carbs1;
@property UIView *fat1;
@property UIView *calori1;



@property (strong, nonatomic) NSArray *data;

@end

@implementation ViewController

-(void)datacallback:(id)Data withIdentifier:(int)Identifier {
           if (Identifier == 1) {
            self.data = Data;
            //NSLog(@"self.data = Data %@", self.data);
            
           }
        else {
            NSLog(@"Error message here");
            
        }
}

-(void)datacallback:(id)Data withIndexPath:(NSIndexPath *)indexPath {
    
}

- (void) viewDidAppear:(BOOL)animated {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.foodListButton.hidden = YES;
    
    Backend *backend = [[Backend alloc] init];
    [backend getAllFoodItems:self indentifier:1];
   
    [self setupUIViews];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.protein, self.fat, self.carbs, self.calori, self.protein1, self.fat1, self.carbs1, self.calori1]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.protein, self.fat, self.carbs, self.calori, self.protein1, self.fat1, self.carbs1, self.calori1]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
   
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    
}

-(void) setupUIViews {
    self.protein = [[UIView alloc] initWithFrame:CGRectMake(100, 0, 75, 75)];
    UIGraphicsBeginImageContext(self.protein.frame.size);
    [[UIImage imageNamed:@"FiberFilled.png"] drawInRect:self.protein.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.protein.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [self.view addSubview:self.protein];
    
    self.carbs = [[UIView alloc] initWithFrame:CGRectMake(200, 0, 150, 150)];
    UIGraphicsBeginImageContext(self.carbs.frame.size);
    [[UIImage imageNamed:@"CarbohydratesFilled.png"] drawInRect:self.carbs.bounds];
    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.carbs.backgroundColor = [UIColor colorWithPatternImage:image1];
    
    [self.view addSubview:self.carbs];
    
    self.fat = [[UIView alloc] initWithFrame:CGRectMake(250, 100, 150, 150)];
    UIGraphicsBeginImageContext(self.fat.frame.size);
    [[UIImage imageNamed:@"LipidsFilled.png"] drawInRect:self.fat.bounds];
    UIImage *image2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.fat.backgroundColor = [UIColor colorWithPatternImage:image2];
    
    [self.view addSubview:self.fat];
    
    self.calori = [[UIView alloc] initWithFrame:CGRectMake(300, 50, 150, 150)];
    UIGraphicsBeginImageContext(self.calori.frame.size);
    [[UIImage imageNamed:@"CaloricFilled.png"] drawInRect:self.calori.bounds];
    UIImage *image3 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.calori.backgroundColor = [UIColor colorWithPatternImage:image3];
    
    [self.view addSubview:self.calori];
    
    self.protein1 = [[UIView alloc] initWithFrame:CGRectMake(50, 50, 75, 75)];
    UIGraphicsBeginImageContext(self.protein1.frame.size);
    [[UIImage imageNamed:@"FiberFilled.png"] drawInRect:self.protein1.bounds];
    UIImage *image4 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.protein1.backgroundColor = [UIColor colorWithPatternImage:image4];
    
    [self.view addSubview:self.protein1];
    
    self.carbs1 = [[UIView alloc] initWithFrame:CGRectMake(300, 90, 100, 100)];
    UIGraphicsBeginImageContext(self.carbs1.frame.size);
    [[UIImage imageNamed:@"CarbohydratesFilled.png"] drawInRect:self.carbs1.bounds];
    UIImage *image5 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.carbs1.backgroundColor = [UIColor colorWithPatternImage:image5];
    
    [self.view addSubview:self.carbs1];
    
    self.fat1 = [[UIView alloc] initWithFrame:CGRectMake(100, 50, 150, 150)];
    UIGraphicsBeginImageContext(self.fat1.frame.size);
    [[UIImage imageNamed:@"LipidsFilled.png"] drawInRect:self.fat1.bounds];
    UIImage *image6 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.fat1.backgroundColor = [UIColor colorWithPatternImage:image6];
    
    [self.view addSubview:self.fat1];
    
    self.calori1 = [[UIView alloc] initWithFrame:CGRectMake(200, 50, 150, 150)];
    UIGraphicsBeginImageContext(self.calori1.frame.size);
    [[UIImage imageNamed:@"CaloricFilled.png"] drawInRect:self.calori1.bounds];
    UIImage *image7 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.calori1.backgroundColor = [UIColor colorWithPatternImage:image7];
    
    [self.view addSubview:self.calori1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"tableSegue"]) {
        TableViewController *tableView = [segue destinationViewController];
        tableView.data = self.data;
        tableView.searchResult = self.data;
       // NSLog(@"self.data array %@", self.data);
        NSLog(@"Segue to tableView");
        
    }
}
@end
