//
//  AppDelegate.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

