//
//  Backend.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "Backend.h"

@implementation Backend

-(NSArray*)getAllFoodItems:(id <DataCallback>)sender indentifier:(int)indentifier {
    self.contentArray = [[NSMutableArray alloc] init];
    NSString *address = [NSString stringWithFormat:@"http://matapi.se/foodstuff?nutrient"];
    NSURL *url = [NSURL URLWithString:address];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Request finished");
        
        if (error) {
            NSLog(@"Error: %@", error);
            [sender datacallback:error withIdentifier: -1];
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if (jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            [sender datacallback:jsonParseError withIdentifier:-1];
            return;
        }
        
        
        //NSLog(@"contentArray: %@", self.contentArray);
        [sender datacallback:result withIdentifier:1];
        
    }];
    
    NSLog(@"Task resume inside Backend");
    [task resume];
    return self.contentArray;
}


-(void)getFoodContent:(id <DataCallback>)sender indexPath:(NSIndexPath*)indexPath {
    
    //NSArray *table = [[NSArray alloc] init];
    //NSDictionary *result = [[NSDictionary alloc] init];
    
   // NSString *address = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%lu?nutrient=", (unsigned long)number];
    //NSLog(@"getFoodContent address: %@", address);
    
    //NSURL *url = [NSURL URLWithString:address];
    
//    NSString *address = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", identifier];
//    NSURL *url = [NSURL URLWithString:address];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    NSURLSession *session = [NSURLSession sharedSession];
//    __block NSDictionary *result = [[NSDictionary alloc] init];
    
    int i = (int) indexPath.row;
    //NSLog(@"indexPath.row %d", i);
    i ++;
    //NSLog(@"indexPath.row + 1 %d", i);
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", i];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error){
                                                
                                                if (error) {
                                                    [sender datacallback:error withIdentifier: -1];
                                                    NSLog(@"Error getFoodContent %@", error);
                                                }
                                                NSError *jsonParseError = nil;
                                                
        NSDictionary *result = [[NSDictionary alloc] init];
        result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if (jsonParseError) {
            NSLog(@"Failed to parse getFoodContent data: %@", jsonParseError);
            [sender datacallback:jsonParseError withIdentifier:-1];
            return;
        }
        
        //NSLog(@"Result getFoodContent result: %@", result);
                                                NSDictionary *nutrientValue = result[@"nutrientValues"];
                                                [sender datacallback:nutrientValue withIndexPath:indexPath];
    }];
    
    [task resume];
    
}


@end
