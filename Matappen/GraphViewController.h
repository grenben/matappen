//
//  GraphViewController.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-04-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface GraphViewController : UIViewController <GKBarGraphDataSource>


@end
