//
//  CellTableViewCell.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-10.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "CellTableViewCell.h"

@implementation CellTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
