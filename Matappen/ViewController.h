//
//  ViewController.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backend.h"
#import "TableViewController.h"
#import "DataCallback.h"

@interface ViewController : UIViewController <DataCallback>


@end

