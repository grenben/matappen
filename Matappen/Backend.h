//
//  Backend.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataCallback.h"
#import <UIKit/UIKit.h>

@interface Backend : NSObject

-(NSArray*)getAllFoodItems:(id <DataCallback>)sender indentifier:(int)indentifier;

-(void)getFoodContent:(id <DataCallback>)sender indexPath:(NSIndexPath *)indexPath;

@property(nonatomic, strong) NSMutableArray *contentArray;

//@property (strong, nonatomic) NSMutableArray *name;


@end
