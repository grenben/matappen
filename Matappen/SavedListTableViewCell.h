//
//  SavedListTableViewCell.h
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-31.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbohydratesLabel;
@property (weak, nonatomic) IBOutlet UILabel *nutritrionLabel;
@property NSString *energyString;
@property NSDictionary *foodDict;

@end
