//
//  main.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-03-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
