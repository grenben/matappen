//
//  GraphViewController.m
//  Matappen
//
//  Created by Johan Grenlund on 2016-04-03.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "GraphViewController.h"

@interface GraphViewController ()
@property (weak, nonatomic) IBOutlet GKBarGraph *graph;


@end

@implementation GraphViewController

- (NSInteger)numberOfBars {
    return 2;
}
- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    return 0;//(int)2;
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    return 0;
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    return 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.graph.dataSource = self;
    [self.graph draw];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
